import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.stat.correlation.PearsonsCorrelation;
import org.apache.commons.math3.stat.descriptive.moment.StandardDeviation;
import org.ojalgo.finance.data.YahooSymbol;
import org.ojalgo.series.CalendarDateSeries;
import org.ojalgo.series.CoordinationSet;
import org.ojalgo.series.primitive.DataSeries;
import org.ojalgo.type.CalendarDateUnit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * Created by Andrei Nagy on 23.01.2017.
 */
public class MarkowitzModelFiWi {
    private static double expectedReturnAsset1;
    private static double expectedReturnAsset2;
    private static double expectedReturnAsset3;

    private static double[][] correlationMatrix;
    private static double[][] covarianceMatrix;

    private static List<Double> standardDeviations;

    private static double[] minimumVariancePortfolio;

    static void fetchDataFromYahooFinance(String asset1, String asset2, String asset3) {
        final YahooSymbol tmpDataAsset1 = new YahooSymbol(asset1, CalendarDateUnit.DAY);
        final YahooSymbol tmpDataAsset2 = new YahooSymbol(asset2, CalendarDateUnit.DAY);
        final YahooSymbol tmpDataAsset3 = new YahooSymbol(asset3, CalendarDateUnit.DAY);

        final CalendarDateSeries<Double> seriesAsset1 = tmpDataAsset1.getPriceSeries();
        final CalendarDateSeries<Double> seriesAsset2 = tmpDataAsset2.getPriceSeries();
        final CalendarDateSeries<Double> seriesAsset3 = tmpDataAsset3.getPriceSeries();

        final CoordinationSet<Double> tmpUncoordinated = new CoordinationSet<>();
        tmpUncoordinated.put("Asset1", seriesAsset1);
        tmpUncoordinated.put("Asset2", seriesAsset2);
        tmpUncoordinated.put("Asset3", seriesAsset3);

        final CoordinationSet<Double> tmpCoordinated = tmpUncoordinated.prune(CalendarDateUnit.DAY);

        final DataSeries tmpDailyDataAsset1 = tmpCoordinated.get("Asset1").getDataSeries();
        final DataSeries tmpDailyDataAsset2 = tmpCoordinated.get("Asset2").getDataSeries();
        final DataSeries tmpDailyDataAsset3 = tmpCoordinated.get("Asset3").getDataSeries();

        // Return of a share based on daily historical data
        double[] returnDailyAsset1 = new double[tmpDailyDataAsset1.size()];
        double[] returnDailyAsset2 = new double[tmpDailyDataAsset2.size()];
        double[] returnDailyAsset3 = new double[tmpDailyDataAsset3.size()];

        for (int i = 1; i < tmpDailyDataAsset1.size(); i++) {
            returnDailyAsset1[i - 1] = Math.log(tmpDailyDataAsset1.value(i) / tmpDailyDataAsset1.value(i - 1));
            returnDailyAsset2[i - 1] = Math.log(tmpDailyDataAsset2.value(i) / tmpDailyDataAsset2.value(i - 1));
            returnDailyAsset3[i - 1] = Math.log(tmpDailyDataAsset3.value(i) / tmpDailyDataAsset3.value(i - 1));
        }

        expectedReturnAsset1 = mean(returnDailyAsset1) * Constants.WORKDAYS;
        expectedReturnAsset2 = mean(returnDailyAsset2) * Constants.WORKDAYS;
        expectedReturnAsset3 = mean(returnDailyAsset3) * Constants.WORKDAYS;

        double volatilityAsset1 = new StandardDeviation().evaluate(returnDailyAsset1) * Math.sqrt(Constants.WORKDAYS);
        double volatilityAsset2 = new StandardDeviation().evaluate(returnDailyAsset2) * Math.sqrt(Constants.WORKDAYS);
        double volatilityAsset3 = new StandardDeviation().evaluate(returnDailyAsset3) * Math.sqrt(Constants.WORKDAYS);

        standardDeviations = new ArrayList<>();
        standardDeviations.add(volatilityAsset1);
        standardDeviations.add(volatilityAsset2);
        standardDeviations.add(volatilityAsset3);
        correlationMatrix = new double[3][3];
        correlationMatrix[0][0] = new PearsonsCorrelation().correlation(returnDailyAsset1, returnDailyAsset1);
        correlationMatrix[1][1] = new PearsonsCorrelation().correlation(returnDailyAsset2, returnDailyAsset2);
        correlationMatrix[2][2] = new PearsonsCorrelation().correlation(returnDailyAsset3, returnDailyAsset3);
        correlationMatrix[0][1] = correlationMatrix[1][0] = new PearsonsCorrelation().correlation(returnDailyAsset1, returnDailyAsset2);
        correlationMatrix[0][2] = correlationMatrix[2][0] = new PearsonsCorrelation().correlation(returnDailyAsset1, returnDailyAsset3);
        correlationMatrix[1][2] = correlationMatrix[2][1] = new PearsonsCorrelation().correlation(returnDailyAsset2, returnDailyAsset3);

        /*covarianceMatrix = new double[3][3];
        covarianceMatrix[0][0] = new Covariance().covariance(returnDailyAsset1, returnDailyAsset1);
        covarianceMatrix[1][1] = new Covariance().covariance(returnDailyAsset2, returnDailyAsset2);
        covarianceMatrix[2][2] = new Covariance().covariance(returnDailyAsset3, returnDailyAsset3);
        covarianceMatrix[0][1] = covarianceMatrix[1][0] = new Covariance().covariance(returnDailyAsset1, returnDailyAsset2);
        covarianceMatrix[0][2] = covarianceMatrix[2][0] = new Covariance().covariance(returnDailyAsset1, returnDailyAsset3);
        covarianceMatrix[1][2] = covarianceMatrix[2][1] = new Covariance().covariance(returnDailyAsset2, returnDailyAsset3);*/

        covarianceMatrix = new double[3][3];
        covarianceMatrix[0][0] = volatilityAsset1 * volatilityAsset1 * correlationMatrix[0][0];
        covarianceMatrix[1][1] = volatilityAsset2 * volatilityAsset2 * correlationMatrix[1][1];
        covarianceMatrix[2][2] = volatilityAsset3 * volatilityAsset3 * correlationMatrix[2][2];

        covarianceMatrix[0][1] = covarianceMatrix[1][0] = volatilityAsset2 * volatilityAsset1 * correlationMatrix[0][1];
        covarianceMatrix[0][2] = covarianceMatrix[2][0] = volatilityAsset3 * volatilityAsset1 * correlationMatrix[0][2];
        covarianceMatrix[1][2] = covarianceMatrix[2][1] = volatilityAsset2 * volatilityAsset3 * correlationMatrix[1][2];

        minimumVariancePortfolio = new double[3];
        minimumVariancePortfolio = MVP(volatilityAsset1, volatilityAsset2, volatilityAsset3, covarianceMatrix[0][1], covarianceMatrix[1][2], covarianceMatrix[0][2]);
    }

    static double getPortfolioValueAtRisk(double risk, double weightAsset1, double weightAsset2, double weightAsset3) {
        NormalDistribution distribution = new NormalDistribution(getExpectedPortfolioReturn(weightAsset1, weightAsset2, weightAsset3), getExpectedPortfolioVolatility_Covariance(weightAsset1, weightAsset2, weightAsset3));
        return distribution.inverseCumulativeProbability(1D - risk);
    }

    static double[] getMinimumVariancePortfolio() {
        return minimumVariancePortfolio;
    }

    public static double getExpectedPortfolioVolatility_Correlation(double weight_apple, double weight_google, double weight_facebook) {
        List<Double> weights = new ArrayList<>();
        weights.add(weight_apple);
        weights.add(weight_google);
        weights.add(weight_facebook);

        return calculateExpectedPortfolioVolatilityUsingCorrelation(3, weights, standardDeviations, correlationMatrix);
    }

    static double getExpectedPortfolioVolatility_Covariance(double weightAsset1, double weightAsset2, double weightAsset3) {
        List<Double> weights = new ArrayList<>();
        weights.add(weightAsset1);
        weights.add(weightAsset2);
        weights.add(weightAsset3);

        return calculateExpectedPortfolioVolatilityUsingCovariance(3, weights, covarianceMatrix);
    }

    static double getExpectedPortfolioReturn(double weightAsset1, double weightAsset2, double weightAsset3) {
        return expectedReturnAsset1 * weightAsset1 + expectedReturnAsset3 * weightAsset3 + expectedReturnAsset2 * weightAsset2;
    }

    private static double mean(double[] m) {
        return Arrays.stream(m).sum() / m.length;
    }

    private static double calculateExpectedPortfolioVolatilityUsingCorrelation(int numberOfAssets, List<Double> weights, List<Double> standardDeviations, double[][] correlationsMatrix) {
        double sum = 0;
        for (int i = 0; i < numberOfAssets; i++)
            for (int j = 0; j < numberOfAssets; j++)
                sum += weights.get(i) * weights.get(j) * standardDeviations.get(i) * standardDeviations.get(j) * correlationsMatrix[i][j];
        return Math.sqrt(sum);
    }

    private static double calculateExpectedPortfolioVolatilityUsingCovariance(int numberOfAssets, List<Double> weights, double[][] covarianceMatrix) {
        double sum = 0;
        for (int i = 0; i < numberOfAssets; i++)
            for (int j = 0; j < numberOfAssets; j++)
                sum += weights.get(i) * weights.get(j) * covarianceMatrix[i][j];
        return sum;
    }

    private static double[] MVP(double sigmaAsset1, double sigmaAsset2, double sigmaAsset3, double covAssets12, double covAssets23, double covAssets13) {
        double a = 2 * sigmaAsset1 * sigmaAsset1;
        double b = 2 * covAssets12;
        double c1 = 2 * covAssets13;

        double d = 2 * covAssets12;
        double e = 2 * sigmaAsset2 * sigmaAsset2;
        double g = 2 * covAssets23;

        double h = 2 * covAssets13;
        double i = 2 * covAssets23;
        double j = 2 * sigmaAsset3 * sigmaAsset3;

        double newa1 = a - d;
        double newb1 = b - e;
        double newc1 = c1 - g;

        double newa2 = a - h;
        double newb2 = b - i;
        double newc2 = c1 - j;

        double newb3 = -newb1 / newa1;
        double newc3 = -newc1 / newa1;

        double newb4 = -newb2 / newa2;
        double newc4 = -newc2 / newa2;

        double newb5 = newb3 - newb4;
        double newc5 = newc4 - newc3;

        double newb6 = newb5 / newc5;

        double newb7 = newc3 * newb6 + newb3;
        double newb8 = newc4 * newb6 + newb4;

        double newb9 = 1 + newb6 + newb7;

        double newb10 = 1 / newb9;
        double newa10 = newb7 * newb10;
        double newc10 = newb6 * newb10;

        double[] weights = new double[3];
        weights[0] = newa10;
        weights[1] = newb10;
        weights[2] = newc10;

        return weights;
    }

}