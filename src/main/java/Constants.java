import java.util.Arrays;
import java.util.List;

/**
 * Created by Andrei Nagy on 23.01.2017.
 */
public class Constants {
    private final static List<String> TICKER_SYMBOL_LIST = Arrays.asList("AAPL", "ADBE", "AMZN", "CERN", "CSCO", "EBAY", "FB", "GOOGL", "NFLX", "TSLA", "TWTR", "YHOO", "ZION");
    public static final int WORKDAYS = 250;

    public static List<String> getTickerSymbolList() {
        return TICKER_SYMBOL_LIST;
    }
}
