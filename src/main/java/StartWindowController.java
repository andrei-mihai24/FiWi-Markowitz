import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class StartWindowController {
    @FXML
    private Label labelAsset1;
    @FXML
    private Label labelAsset2;
    @FXML
    private Label labelAsset3;
    @FXML
    private Button startButton;

    public void changeAsset1ButtonClicked() {
        ChangeDefaultAssets.display();
        labelAsset1.setText(ChangeDefaultAssets.getChosenTicker());
    }

    public void changeAsset2ButtonClicked() {
        ChangeDefaultAssets.display();
        labelAsset2.setText(ChangeDefaultAssets.getChosenTicker());
    }

    public void changeAsset3Button() {
        ChangeDefaultAssets.display();
        labelAsset3.setText(ChangeDefaultAssets.getChosenTicker());
    }

    public void exitButtonClicked() {
        System.exit(0);
    }

    public void startButtonClicked() {
        MarkowitzModelFiWi.fetchDataFromYahooFinance(labelAsset1.getText(), labelAsset2.getText(), labelAsset3.getText());
        double[] minimumVariancePortfolio = MarkowitzModelFiWi.getMinimumVariancePortfolio();
        double mvp_volatility = MarkowitzModelFiWi.getExpectedPortfolioVolatility_Covariance(minimumVariancePortfolio[0], minimumVariancePortfolio[1], minimumVariancePortfolio[2]);
        double mvp_return = MarkowitzModelFiWi.getExpectedPortfolioReturn(minimumVariancePortfolio[0], minimumVariancePortfolio[1], minimumVariancePortfolio[2]);
        double[] mvp_point = new double[2];
        mvp_point[0] = mvp_return;
        mvp_point[1] = mvp_volatility;

        System.out.println("Portfolio value at risk 10%  " + MarkowitzModelFiWi.getPortfolioValueAtRisk(0.1, 0.33, 0.33, 0.34));

        List<Double> points_asset1 = generatePointsUsingRandomNumbers(500, "APPLE");
        List<Double> points_asset2 = generatePointsUsingRandomNumbers(500, "GOOGLE");
        List<Double> points_asset3 = generatePointsUsingRandomNumbers(500, "FACEBOOK");

        ScatterChartMarkowitz.display((Stage) startButton.getScene().getWindow(), labelAsset1.getText(), labelAsset2.getText(), labelAsset3.getText(), points_asset1, points_asset2, points_asset3, mvp_point);
    }

    // Generates three random positive numbers that sum up to 1
    private static List<Double> getNextRandom() {
        Random r = new Random();
        List<Integer> load = new ArrayList<>();
        load.add(r.nextInt(5000));
        load.add(r.nextInt(5000));

        double factor = load.get(0) + load.get(1);
        List<Double> randomPercentages = new ArrayList<>();
        randomPercentages.add(load.get(0) / factor);
        randomPercentages.add(load.get(1) / factor);

        return randomPercentages;

        /*  For 3 random numbers

        Random r = new Random();
        List<Integer> load = new ArrayList<>();
        load.add(r.nextInt(5000));
        load.add(r.nextInt(5000));
        load.add(r.nextInt(5000));
        double factor = load.get(0) + load.get(1) + load.get(2);
        List<Double> randomPercentages = new ArrayList<>();
        randomPercentages.add(load.get(0) / factor);
        randomPercentages.add(load.get(1) / factor);
        randomPercentages.add(load.get(2) / factor);

        return randomPercentages;*/
    }

    private static List<Double> generatePointsUsingRandomNumbers(int howManyPoints, String whichOneIsZero) {
        double volatility = 0D;
        double expectedReturn = 0D;
        List<Double> randomNumbers;
        List<Double> points = new ArrayList<>();

        for (int i = 0; i < howManyPoints; i++) {
            randomNumbers = getNextRandom();
            double sum = randomNumbers.get(0) + randomNumbers.get(1);

            switch (whichOneIsZero) {
                case "APPLE":
                    volatility = MarkowitzModelFiWi.getExpectedPortfolioVolatility_Covariance(0, randomNumbers.get(0), randomNumbers.get(1));
                    expectedReturn = MarkowitzModelFiWi.getExpectedPortfolioReturn(0, randomNumbers.get(0), randomNumbers.get(1));
                    break;
                case "GOOGLE":
                    volatility = MarkowitzModelFiWi.getExpectedPortfolioVolatility_Covariance(randomNumbers.get(0), 0, randomNumbers.get(1));
                    expectedReturn = MarkowitzModelFiWi.getExpectedPortfolioReturn(randomNumbers.get(0), 0, randomNumbers.get(1));
                    break;
                case "FACEBOOK":
                    volatility = MarkowitzModelFiWi.getExpectedPortfolioVolatility_Covariance(randomNumbers.get(0), randomNumbers.get(1), 0);
                    expectedReturn = MarkowitzModelFiWi.getExpectedPortfolioReturn(randomNumbers.get(0), randomNumbers.get(1), 0);
                    break;
            }

            points.add(expectedReturn);
            points.add(volatility);

            System.out.println(randomNumbers.toString() + "  ---  SUM: " + sum + "  return: " + expectedReturn + " vol: " + volatility);
        }

        return points;
    }

    /*private static List<Double> generatePointsUsingRandomNumbers(int howManyPoints)
    {
        double volatility;
        double expectedReturn;
        List<Double> randomNumbers;
        List<Double> points = new ArrayList<>();

        for (int i = 0; i < howManyPoints; i++)
        {
            randomNumbers = getNextRandom();
            double sum = randomNumbers.get(0) + randomNumbers.get(1) + randomNumbers.get(2);
            System.out.println(randomNumbers.toString() + "  ---  SUM: " + sum);
            volatility = MarkowitzModelFiWi.getExpectedPortfolioVolatility_Covariance(randomNumbers.get(0), randomNumbers.get(1), randomNumbers.get(2));
            expectedReturn = MarkowitzModelFiWi.getExpectedPortfolioReturn(randomNumbers.get(0), randomNumbers.get(1), randomNumbers.get(2));
            points.add(expectedReturn);
            points.add(volatility);
        }

        return points;
    }*/
}

