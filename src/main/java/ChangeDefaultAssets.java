import javafx.scene.control.ChoiceDialog;

import java.util.Optional;

/**
 * Created by Andrei Nagy on 23.01.2017.
 */
public class ChangeDefaultAssets {
    private static String chosenTicker;

    public static void display() {
        ChoiceDialog<String> dialog = new ChoiceDialog<>(Constants.getTickerSymbolList().get(0), Constants.getTickerSymbolList());
        dialog.setTitle("List of companies (ticker symbols)");
        dialog.setHeaderText("There are " + Constants.getTickerSymbolList().size() + " companies to choose from");
        dialog.setContentText("Choose one:");

        Optional<String> result = dialog.showAndWait();

        result.ifPresent(choice -> chosenTicker = choice);
    }

    public static String getChosenTicker() {
        return chosenTicker;
    }
}
